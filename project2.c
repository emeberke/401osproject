#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/wait.h>

char ** parse(char * inputString, char * delimiter);
char * parseString(char * delimiter, char * inputString);
void pointerPrint(char ** argv);
char * checkString(char ** parsedInput, char ** path, char * input);
void exec(char * pathToFile, char ** parsedInput);

int main(int argc, char ** argv, char ** envp){ 
	char * pathDelimiter = ":";
	char * inputDelimiter = " ";
	char * pathString;
	char * input = malloc(200);
	char ** path = 0;
	char ** parsedInput = 0;
	char * pathToFile = malloc(200);

	for(int i = 0; envp[i] != NULL; i++) {
		char *searchString = envp[i];
		if(searchString != NULL && searchString[0] == 'P' && searchString[1] == 'A') {
			pathString = malloc(sizeof(char) * strlen(envp[i]));
			pathString = envp[i];
		}
	}
	path = malloc(sizeof(char*) * strlen(pathString));
	path = parse(pathString, pathDelimiter);
	pointerPrint(path);

	while(1) {
		printf("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n\n");
		printf("SUPER BASH $");
		scanf("%[^\n]%*c", input);

		parsedInput = malloc(sizeof(char*) * strlen(input));
		parsedInput = parse(input, inputDelimiter);

		pathToFile = checkString(parsedInput, path, input);
		pointerPrint(parsedInput);

		exec(pathToFile, parsedInput);
	}
}

char ** parse(char * inputString, char * delimiter) {
	char * countString = inputString;
	size_t count = 0;
	char * last_delimiter = 0;
	char ** parsedString = 0;

	while(*countString) {
		if(*delimiter == *countString) {
			count ++;
			last_delimiter = countString;
		}
		countString ++;
	}

	count += last_delimiter < (inputString + strlen(inputString) - 1);

	count ++;

	parsedString = malloc(sizeof(char*) * count);

	if(parsedString) {
		size_t index = 0;
		char* token = parseString(delimiter, inputString);

		while (token) {
			assert(index < count);
			*(parsedString + index++) = strdup(token);
			token = parseString(delimiter, 0);
		}
		assert(index == count - 1);
		*(parsedString + index) = 0;
	}

	return parsedString;
}

char * parseString(char * delimiter, char * inputString){
	static char *lasts;
	register int ch;

	if(inputString == 0)
		inputString = lasts;
	do {
		if ((ch = *inputString++) == '\0')
			return 0;
	} while (strchr(delimiter, ch));
	--inputString;
	lasts = inputString + strcspn(inputString, delimiter);
	if(*lasts != 0)
		*lasts++ = 0;
	return inputString;
}

void pointerPrint(char ** path){
	printf("Address %p \n", &path);

	if(path) {
		for(int i = 0; *(path + i); i++) {
		   char * pointer = *(path + i);
		   printf("Base[%d] Address %p Pointer Value %p String=%s\n", i, (path +i), pointer, *(path + i));
		}
		printf("\n");
	}
}

char * checkString(char ** parsedInput, char ** path, char * input){ 
	char * concatString = 0;
	int numPointers = 0;
	char * enteredString;
	for(int i = 0; path[i] != NULL; i++) {
		char * searchString = malloc(sizeof(char) * strlen(path[i]));
		searchString = strdup(path[i]);
		if(searchString != NULL && searchString[0] == 'P' && searchString[1] == 'A') {
			concatString = malloc(sizeof(char*) * strlen(searchString));
			concatString = searchString;
			strcat(concatString, "/");
			strcat(concatString, input);
		}
	}
	while(parsedInput[numPointers] != NULL) {
		numPointers++;
	}

	enteredString = malloc(sizeof(char*) * numPointers);
	
	while (concatString != NULL) {
		printf("\nChecking %s", concatString);
		if(access(concatString, F_OK) != -1) {
			printf("\nFound!");
			for(int i = 0; i < numPointers; i++) {
				enteredString = strcat(enteredString, " ");
				enteredString = strcat(enteredString, parsedInput[i]);
			}
			printf("\nString = '%s'", enteredString);
			printf("\nNumber of pointers = %d\n", numPointers+1);
			return concatString;
		}
		else {
			concatString++;
		}
		if(concatString[0] != '/'){
			while(concatString[0] != '/') {
				concatString++;
			}
		}
	}
}

void exec(char * pathToFile, char ** parsedInput){ 
	int pid;

	pid = fork();
	if(pid == 0) {
		execv(pathToFile, parsedInput);
		printf("\nexecv failed\n");
	}
	else {
		wait(&pid);
	}
}
