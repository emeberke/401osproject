#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

char * splitAndFindLength(char *delimiter, char *inputString);

int main(int numInput, char *inputArray[]) {
	char *delimiter = inputArray[1];
	char *inputString = inputArray[2];
	char **strings = 0;
	char *countString = inputString;
	size_t count = 0;
	char *last_delimiter = 0;

	if(numInput == 3) {
		while(*countString) {
			if(*delimiter == *countString) {
				count ++;
				last_delimiter = countString;
			}
			countString++;
		}

		count += last_delimiter < (inputString + strlen(inputString) - 1);

		count++;

		strings = malloc(sizeof(char*) * count);

		if(strings) {
			size_t index = 0;
			char* token = splitAndFindLength(delimiter, inputString);

			while (token) {
				assert(index < count);
				*(strings + index++) = strdup(token);
				token = splitAndFindLength(delimiter, 0);
			}
			assert(index == count - 1);
			*(strings + index) = 0;
		}
	}
	else if(numInput > 3) {
		printf("Too many arguments supplied.\n");
	}
	else {
		printf("Two arguments expected.\n");
	}

	if (strings) {
		for(int i = 0; *(strings + i); i++) {
			int length = strlen(*(strings + i));
			printf("%s Length %d\n", *(strings + i), length);
			free(*(strings + i));
		}
		printf("\n");
		free(strings);
	}
	return 0;
}

char * splitAndFindLength(char *delimiter, char *inputString) {

	static char *lasts;
	register int ch;

	if(inputString == 0)
		inputString = lasts;
	do {
		if ((ch = *inputString++) == '\0')
			return 0;
	} while (strchr(delimiter, ch));
	--inputString;
	lasts = inputString + strcspn(inputString, delimiter);
	if(*lasts != 0)
		*lasts++ = 0;
	return inputString;
}
